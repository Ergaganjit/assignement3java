import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
public class AirRideLimo extends Application{

	public static void main(String[] args) {
		launch (args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		Label heading = new Label("Airport Ride Calculator");
		Label headingFrom = new Label("From:  (Choose between Cestar College or Brampton only)  ");
		TextField textBoxFrom = new TextField();
		//RadioButton radioButton1=new RadioButton("Cestar College");
		//RadioButton radioButton2=new RadioButton("Brampton");

		CheckBox checkBox1=new CheckBox("Extra luggage?");
		CheckBox checkBox2=new CheckBox("Pets");
		CheckBox checkBox3=new CheckBox("Use 407 ETR?");
		CheckBox checkBox4=new CheckBox("Add Tip?");
		Button calculateButton = new Button();
		calculateButton.setText("CALCULATE");
		TextField result = new TextField();

		

		calculateButton.setOnAction(new EventHandler<ActionEvent>() {
		   @Override
		   public void handle(ActionEvent e) {
		   	
		           double baseFare=0;
		           double kilometer=0;
		           double additionCharges=0;
		   	//String textFrom =textBoxFrom.getText();
		   	String textBoxText=textBoxFrom.getText();
		   	
		   	
		   	if(textBoxText=="Cestar College" ||textBoxText=="Brampton") {
		   	if(textBoxText=="Cestar College")
		   	{
		   	baseFare=51.13;
		   	kilometer=7;
		   	
		   	}
		   	if(textBoxText=="Brampton")
		   	{
		   	baseFare=38.13;
		   	kilometer=10;
		   	}
		   	
		   	
		   	if(checkBox1.isSelected())
		   	{
		   	additionCharges=10;
		   	}
		   	if (checkBox2.isSelected())
		   	{
		   	additionCharges=6;
		   	}
		   	if (checkBox3.isSelected())
		   	{
		   	additionCharges=25*kilometer;
		   	}
		   	double fare=baseFare+additionCharges;
		   	double tax=fare*0.13;
		   	double total=fare+tax;
		   	double tip=0.00;
		   	if(checkBox4.isSelected())
		   	{
		   	tip=total*0.15;
		   	}
		   	double totalPrice=total+tip;
		   	
		   	
		   	result.setText("The total fare is : " + totalPrice);}
		   	else {
		   		result.setText("Invalid Option, Enter Location between Cestar College and Brampton only");
		   	}
		   	
		   }
		});


		VBox root = new VBox();
		root.setSpacing(10);	

		
		root.getChildren().add(heading);
		root.getChildren().add(headingFrom);
		root.getChildren().add(textBoxFrom);
	//	root.getChildren().add(radioButton1);
		//root.getChildren().add(radioButton2);


		root.getChildren().add(checkBox1);
		root.getChildren().add(checkBox2);
		root.getChildren().add(checkBox3);
		root.getChildren().add(checkBox4);
		root.getChildren().add(calculateButton);
		root.getChildren().add(result);




		primaryStage.setScene(new Scene(root, 250, 300));



		primaryStage.setTitle("AirRideLimo Calculator");

		primaryStage.show();

	}

}
